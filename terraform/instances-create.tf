resource "aws_key_pair" "deployer" {
  key_name   = "deployer"
  public_key = "${var.public_key}"
}
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

module "ec2" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 1.0"

  name           = "${local.cluster}"
  instance_count = "${local.instance_count}"

  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  key_name                    = "${aws_key_pair.deployer.key_name}"
  monitoring                  = true
  vpc_security_group_ids      = ["${aws_security_group.web.id}"]
  subnet_id                   = "${element(module.vpc.public_subnets, 0)}"
  associate_public_ip_address = true

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

resource "aws_lb_target_group_attachment" "http" {
  count            = "${local.instance_count}"
  target_group_arn = "${module.alb.target_group_arns[0]}"
  target_id        = "${module.ec2.id[count.index]}"
  port             = 80
}

# resource "aws_lb_target_group_attachment" "https" {
#   count            = "${local.instance_count}"
#   target_group_arn = "${module.alb.target_group_arns[1]}"
#   target_id        = "${module.ec2.id[count.index]}"
#   port             = 443
# }

resource "aws_efs_file_system" "efs" {
  creation_token = "EFS for ${local.cluster}"
 
  tags {
    Name = "EFS for ${local.cluster}"
  }
}
resource "aws_efs_mount_target" "efsMount" {
  count = "2"
  file_system_id = "${aws_efs_file_system.efs.id}"
  subnet_id      = "${element(module.vpc.public_subnets, count.index)}"
  security_groups = ["${aws_security_group.web.id}"]
}
 
