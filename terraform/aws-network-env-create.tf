provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}

data "aws_availability_zones" "available" {}

provider "random" {
  version = "= 1.1.0"
}

resource "random_string" "suffix" {
  length  = 8
  special = false
}


module "vpc" {
  source                  = "terraform-aws-modules/vpc/aws"
  version                 = "1.14.0"
  name                    = "${local.cluster}"
  cidr                    = "10.0.0.0/16"
  azs                     = ["${data.aws_availability_zones.available.names[0]}", "${data.aws_availability_zones.available.names[1]}"]
  private_subnets         = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets          = ["10.0.3.0/24", "10.0.4.0/24"]
  enable_nat_gateway      = true
  single_nat_gateway      = true
  map_public_ip_on_launch = true
  enable_dns_hostnames    = true
  enable_dns_support      = true
  tags                    = "${local.tags}"
}

resource "aws_iam_server_certificate" "cert" {
  name             = "${local.cluster}_self_signed_cert-${random_string.suffix.result}"
  certificate_body = "${tls_self_signed_cert.cert.cert_pem}"
  private_key      = "${tls_private_key.cert.private_key_pem}"
  lifecycle {
    create_before_destroy = true
  }
}

module "alb" {
  source                   = "terraform-aws-modules/alb/aws"
  version                  = "~> 3.0"
  load_balancer_name       = "${local.cluster}"
  security_groups          = ["${aws_security_group.web.id}"]
  logging_enabled          = false
  subnets                  = "${module.vpc.public_subnets}"
  vpc_id                   = "${module.vpc.vpc_id}"
  tags                     = "${local.tags}"
  https_listeners          = "${local.https_listeners}"
  https_listeners_count    = "${local.https_listeners_count}"
  http_tcp_listeners       = "${local.http_tcp_listeners}"
  http_tcp_listeners_count = "${local.http_tcp_listeners_count}"
  target_groups            = "${local.target_groups}"
  target_groups_count      = "${local.target_groups_count}"
}
resource "aws_security_group" "web" {
  name        = "${local.cluster} web"
  description = "${local.cluster} web hosts sg"
  vpc_id      = "${module.vpc.vpc_id}"
}
resource "aws_security_group_rule" "allow_http" {
  type            = "ingress"
  from_port       = 80
  to_port         = 80
  protocol        = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.web.id}"
}

resource "aws_security_group_rule" "allow_https" {
  type            = "ingress"
  from_port       = 443
  to_port         = 443
  protocol        = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.web.id}"
}

resource "aws_security_group_rule" "allow_nfs" {
  type            = "ingress"
  from_port       = 2049
  to_port         = 2049
  protocol        = "tcp"
  self            = true

  security_group_id = "${aws_security_group.web.id}"
}

resource "aws_security_group_rule" "allow_ssh" {
  type            = "ingress"
  from_port       = 22
  to_port         = 22
  protocol        = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.web.id}"
}

resource "aws_security_group_rule" "allow_outbound" {
  type            = "egress"
  from_port       = 0
  to_port         = 0
  protocol        = "-1"
  cidr_blocks     = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.web.id}"
}

resource "aws_security_group" "db" {
  name        = "${local.cluster}"
  description = "${local.cluster} database sg"
  vpc_id      = "${module.vpc.vpc_id}"
}

resource "aws_security_group_rule" "allow_db_inbound" {
  type            = "ingress"
  from_port       = 3306
  to_port         = 3306
  protocol        = "tcp"
  source_security_group_id = "${aws_security_group.web.id}"
  security_group_id = "${aws_security_group.db.id}"
}

resource "aws_security_group_rule" "allow_db_outbound" {
  type            = "egress"
  from_port       = 0
  to_port         = 0
  protocol        = "-1"
  cidr_blocks     = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.db.id}"
}
