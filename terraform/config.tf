variable "aws_access_key" {
    default = ""
}
variable "aws_secret_key" {
    default = ""
}
variable "public_key" {
    type = "string"
}

terraform {
  backend "s3" {
      key    = "terraform-state"
  }
}
